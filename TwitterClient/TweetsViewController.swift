//
//  TweetsViewController.swift
//  TwitterClient
//
//  Created by Drew Kim on 3/21/15.
//  Copyright (c) 2015 Drew Kim. All rights reserved.
//

import UIKit
import Social
import Accounts
import SwifteriOS

class TweetsViewController: UITableViewController {
    
    var swifter:Swifter
    let useACAccount = true
    var tweets : [JSONValue] = []
    
    required init(coder aDecoder: NSCoder)
    {
        self.swifter = Swifter(consumerKey: "TnZUNXJ5eLiSW2w3T6W0Ux4eA", consumerSecret: "RZ5hbOWnrScOGGGrhTh0ZbxuArdPPhoJPPavuN3mIzG3Yy2pC2")
        super.init(coder: aDecoder)
        authenticate()
    }

    override func viewDidLoad()
    {
        super.viewDidLoad()
        var refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: Selector("update"), forControlEvents: UIControlEvents.ValueChanged)
        self.refreshControl = refreshControl
    }
    
    func authenticate()
    {
        let failureHandler: ((NSError) -> Void) = {
            error in
            self.alert("Error", message: error.localizedDescription)
        }
        
        if useACAccount
        {
            let accountStore = ACAccountStore()
            let accountType = accountStore.accountTypeWithAccountTypeIdentifier(ACAccountTypeIdentifierTwitter)
            
            accountStore.requestAccessToAccountsWithType(accountType, options: nil) {
                granted, error in
                
                if granted
                {
                    let twitterAccounts = accountStore.accountsWithAccountType(accountType)
                    
                    if twitterAccounts?.count == 0
                    {
                        self.alert("Error", message: "No Twitter accounts")
                    }
                    else
                    {
                        let twitterAccount = twitterAccounts[0] as ACAccount
                        self.swifter = Swifter(account: twitterAccount)
                        self.fetchStream()
                    }
                }
                else
                {
                    self.alert("Error", message: error.localizedDescription)
                }
            }
        }
        else
        {
            swifter.authorizeWithCallbackURL(NSURL(string: "tclient://success")!, success: {
                accessToken, response in
                self.fetchStream()
                },failure: failureHandler
            )
        }
    }
    
    func fetchStream()
    {
        let failureHandler: ((NSError) -> Void) = {
            error in
            self.alert("Error", message: error.localizedDescription)
        }
        
        self.swifter.getStatusesHomeTimelineWithCount(30, sinceID: nil, maxID: nil, trimUser: true, contributorDetails: false, includeEntities: true, success: {
            (statuses: [JSONValue]?) in
            
            if statuses != nil
            {
                self.tweets = statuses!
            }
            }, failure: failureHandler)
        
    }

    func update()
    {
        fetchStream()
        refreshControl?.endRefreshing()
        tableView.reloadData()
    }
    
    override func viewWillLayoutSubviews()
    {
        super.viewWillLayoutSubviews()
        self.tableView.contentInset = UIEdgeInsetsMake(self.topLayoutGuide.length, 0, 0, 0)
        self.tableView.scrollIndicatorInsets = UIEdgeInsetsMake(self.topLayoutGuide.length, 0, 0, 0)
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return tweets.count
    }
    
    func alert(subject: String, message: String)
    {
        var alert = UIAlertController(title: subject, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "OK", style: .Default, handler: nil))
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = UITableViewCell(style: .Subtitle, reuseIdentifier: nil)
        cell.textLabel?.numberOfLines = 5
        cell.textLabel?.text = tweets[indexPath.row]["text"].string
        return cell
    }
    
}
